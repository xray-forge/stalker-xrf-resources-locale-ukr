## [XRF engine](https://github.com/xray-forge/stalker-xrf-engine)

Part of [XRF engine](https://github.com/xray-forge/stalker-xrf-engine) building pipeline.

## Ukrainian locale resources of stalker xrf template 

Only folders in `src/resources` are transformed as assets for xrf template. <br/>
Used to handle textures/sounds/meshes and other statics.
